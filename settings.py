import os
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database


GATEWAY_URL = os.getenv('GATEWAY_URL', '192.168.96.1')
MYSQL_URI = f'mysql://root:pass@{GATEWAY_URL}/bank'

engine = create_engine(MYSQL_URI)
if not database_exists(engine.url):
    create_database(engine.url)

