FROM python:3.7

COPY . /best_bank
WORKDIR /best_bank

COPY requirements.txt ./
RUN pip install -r requirements.txt

