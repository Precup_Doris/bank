# bank



## Description
Bank is an API that handles money transfer to an unreliable bank.
It contains:
- an unreliable API - `shitty bank` container
- a light Proxy on top of it - `bank` container
- an SQL database - `db` container
- and a service that makes requests - `bank-requests` container

`bank` service will get al the requests, save them in `db` from where it will be taken and processed by `bank-requests` which will forward them to `shitty-bank`.
In this way, the service will return immediately and will make sure the `shitty-bank` will process eventually, every request.
For the ease of testing, a wallet is already setup with the iban DE27100777770209299700 and amount 1000, so we won't have problems with transferring money from an empty account.

This API contains a single endpoint POST `/transfer` which takes the following data:
```bash
{
  "from_iban": <str>,
  "to_iban": <str>,
  "amount": <str> (int but as a string)
}
```

This endpoint will save a wallet for each given iban and it's equivalent amount to database and will return immediately.


## Setup
In order to facilitate it's testing, this project is using docker
```bash
docker-compose up --build
```
There may be connection errors, so in order to avoid that, find out the `GATEWAY_URL` from inspecting `db` container
```bash
docker inspect db
```
set it in `settings.py` file 
```bash
GATEWAY_URL = os.getenv('GATEWAY_URL', '192.168.96.1')
```
then rebuild `bank` and `bank-requests` containers to get the new url.

## Usage
```bash
curl -X POST -H "Content-Type: application/json" -d '{"from_iban": "DE27100777770209299700","to_iban": "DE11520513735120710136","amount": "1000"}' http://0.0.0.0:8001/transfer
```
