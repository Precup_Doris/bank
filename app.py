from uuid import uuid4

from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy

from settings import MYSQL_URI

app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI'] = MYSQL_URI
db = SQLAlchemy(app)


class Wallet(db.Model):
    __tablename__ = 'wallets'
    iban = db.Column(db.String(40), primary_key=True, unique=True)
    wallet_id = db.Column(db.String(40), unique=True)
    amount = db.Column(db.Integer, default=0)
    transaction_in_progress = db.Column(db.Boolean, default=False)
    pending_amount = db.Column(db.Integer, default=0)

    def __init__(self, iban):
        self.iban = iban
        self.wallet_id = str(uuid4())


def setup_db():

    db.create_all()
    iban = 'DE27100777770209299700'
    if not Wallet.query.get(iban):
        wallet = Wallet(iban=iban)
        wallet.wallet_id = '5f284bf3-1277-474f-b19f-913ec627f431'
        db.session.add(wallet)
    else:
        wallet = Wallet.query.get(iban)
    wallet.amount = 1000
    wallet.pending_amount = 1000
    db.session.commit()


setup_db()

class Transfer(Resource):

    def post(self):
        data = request.json
        sender_iban = data['from_iban']
        receiver_iban = data['to_iban']
        amount = int(data['amount'])

        create_wallet_if_doesnt_exist(receiver_iban)
        change_amount(receiver_iban, amount)

        create_wallet_if_doesnt_exist(sender_iban)
        change_amount(sender_iban, -amount)

        return jsonify(dict(result='success'))



def create_wallet_if_doesnt_exist(iban):
    if not Wallet.query.get(iban):
        new_wallet = Wallet(iban=iban)
        db.session.add(new_wallet)
        db.session.commit()


def change_amount(iban, amount):
    wallet = Wallet.query.get(iban)

    future_amount = eval('wallet.amount+amount')
    if future_amount < 0:
        raise Exception('Not enough founds!')

    wallet.amount = future_amount
    wallet.pending_amount = eval('wallet.pending_amount+amount')
    db.session.commit()


api.add_resource(Transfer, '/transfer')

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')

