import asyncio
from datetime import datetime, timedelta

import requests
from sqlalchemy import or_

from app import Wallet, db
from settings import GATEWAY_URL, engine

SHITTY_BANK_URL = f'http://{GATEWAY_URL}:5001'
BLOCKED_UNTIL = datetime(year=2020, month=12, day=12)
WAIT_TIME = 5
RETRY_TIMES = 3
EVENT_ID_TO_CHECK = 1
CHECKED_EVENTS = {}

session = requests.session()


def compute_blocked_time():
    global BLOCKED_UNTIL
    if not BLOCKED_UNTIL:
        BLOCKED_UNTIL = datetime.now()
    BLOCKED_UNTIL += timedelta(hours=1)


def save_wallet(wallet):
    retry = RETRY_TIMES
    while retry:
        response = session.post(url=f'{SHITTY_BANK_URL}/wallet/{wallet.wallet_id}')
        if response.status_code == 500:
            compute_blocked_time()
            raise ConnectionError(f'Wallet {wallet.wallet_id} could not be saved. Requests may be blocked!')
        else:
            status = response.json()['result']
            if status == 'error':
                return
        retry -= 1


def transfer_money(wallet, operation_type):
    data = {
        "wallet_id": wallet.wallet_id,
        "type": operation_type,
        "iban": wallet.iban,
        "amount": str(wallet.pending_amount)
    }

    response = session.post(url=f'{SHITTY_BANK_URL}/settle', json=data)
    wallet.transaction_in_progress = True
    db.session.commit()

    if response.status_code == 500:
        compute_blocked_time()
        raise ConnectionError('Transfer could not be made. Requests may be blocked!')

    check_event(wallet)


def check_event(wallet):
    retry = RETRY_TIMES
    while retry:
        event_id = None
        if not CHECKED_EVENTS:
            get_events()

        for event_id, event in CHECKED_EVENTS.items():
            if event['wallet_id'] == wallet.wallet_id and event['amount'] == wallet.pending_amount:
                wallet.pending_amount = 0
                wallet.transaction_in_progress = False
                db.session.commit()
                break

        if event_id:
            CHECKED_EVENTS.pop(event_id)
            break
        retry -= 1


def get_events():
    global EVENT_ID_TO_CHECK

    retry = RETRY_TIMES
    while retry:
        event_ids = list()
        response = session.get(url=f'{SHITTY_BANK_URL}/events/{EVENT_ID_TO_CHECK}')
        if response.status_code == 500:
            compute_blocked_time()
            raise ConnectionError('Transfer could not be made. Requests may be blocked!')

        events = response.json().get('events')
        if isinstance(events, list):
            for event in events:
                if event['event_id'] in CHECKED_EVENTS:
                    continue

                if len(event['wallet_id']) > 20:
                    CHECKED_EVENTS[event['event_id']] = event
                event_ids.append(event['event_id'])
            EVENT_ID_TO_CHECK = get_event_id_to_check(event_ids) or EVENT_ID_TO_CHECK
        retry -= 1


def get_event_id_to_check(events):
    sorted_ids = sorted(events)
    for index, number in enumerate(sorted_ids[:-1]):
        if sorted_ids[index] + 1 != sorted_ids[index + 1]:
            return sorted_ids[index] + 1
    return 0



from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind = engine)

async def make_requests():
    session = Session()
    while True:
        if BLOCKED_UNTIL < datetime.now():
            #for wallet in Wallet.query.filter(or_(Wallet.created==False,Wallet.pending_amount!=0)):
            for wallet in session.query(Wallet).filter(or_(Wallet.transaction_in_progress==True,Wallet.pending_amount!=0)):
                try:
                    save_wallet(wallet)
                    if wallet.pending_amount != 0:
                        if not wallet.transaction_in_progress:
                            transfer_type = 'payin' if wallet.pending_amount > 0 else 'payout'
                            transfer_money(wallet, transfer_type)
                        else:
                            check_event(wallet)
                except ConnectionError:
                    break

        await asyncio.sleep(3)


loop = asyncio.get_event_loop()
loop.create_task(make_requests())

loop.run_forever()